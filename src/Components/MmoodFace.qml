import QtQuick.Shapes

import com.mediklik.capriccio
import QtQuick

Rectangle {
  id: moodRect

  property alias moodType: mood.type

  width: Math.min(parent.width, parent.height) * 0.8; height: width
  radius: width / 2
  color: "yellow"

  Mmood { // C++ defined
    id: mood
  }

  Rectangle { // left eye
    id : leftEye
    x: moodRect.width * 0.25; y: moodRect.height * 0.25
    width: moodRect.width * 0.05; height: width; radius: width / 2
    color: "#fff"

    SequentialAnimation {
                id: blinkAnimation
                loops: Animation.Infinite

                PropertyAnimation {
                    target: leftEye
                    property: "height"
                    to: 0  // Fully close the eye
                    duration: 150
                    easing.type: Easing.OutQuad
                }

                PropertyAnimation {
                    target: leftEye
                    property: "height"
                    to: leftEye.width  // Return to normal height
                    duration: 150
                    easing.type: Easing.OutQuad
                }
            }
        }

        Timer {
            interval: 3000  // Interval in milliseconds (adjust as needed)
            repeat: true
            running: true

            onTriggered: {
                blinkAnimation.start()
            }
        }


  Rectangle { // right eye
    id: rightEye
    x: moodRect.width * 0.75 // FIXME!!
    y: moodRect.height * 0.25
    width: moodRect.width * 0.05; height: width; radius: width / 2
    color: "#fff"


    SequentialAnimation {
                id: blinkAnimation2
                loops: Animation.Infinite

                PropertyAnimation {
                    target: rightEye
                    property: "height"
                    to: 0  // Fully close the eye
                    duration: 150
                    easing.type: Easing.OutQuad
                }

                PropertyAnimation {
                    target: rightEye
                    property: "height"
                    to: rightEye.width  // Return to normal height
                    duration: 150
                    easing.type: Easing.OutQuad
                }
            }
        }

        Timer {
            interval: 3000  // Interval in milliseconds (adjust as needed)
            repeat: true
            running: true

            onTriggered: {
                blinkAnimation2.start()
            }
        }


  Shape {
    id: snoot
    anchors.fill: parent
    ShapePath {
      fillColor: "transparent"
      capStyle: ShapePath.RoundCap
      strokeColor: "#fff"
      strokeWidth: moodRect.width / 50
      startX: snoot.width / 2; startY: snoot.height * 0.4
      PathLine { relativeX: -snoot.width * 0.05; relativeY: snoot.height * 0.2 }
      PathLine { relativeX: snoot.width * 0.05; relativeY: -snoot.height * 0.05 }
    }
  }

  Shape {
    id: smile
    property real yFactor: height / (Mmood.MoodCount - 1)
    x: moodRect.width * 0.2
    y: moodRect.height - height - smile.yFactor * mood.type / 2.0
    width: moodRect.width * 0.6; height: width * 0.5
    ShapePath {
      fillColor: "transparent"
      capStyle: ShapePath.RoundCap
      strokeColor: "#fff"
      strokeWidth: moodRect.width / 50
      startX: 0; startY: smile.yFactor * mood.type
      PathQuad {
        x: smile.width; y: smile.yFactor * mood.type
        controlX: smile.width / 2
        controlY: smile.height - smile.yFactor * mood.type
      }
    }
    ShapePath { // mouth corners - only when very happy :-)
      fillColor: "transparent"
      capStyle: ShapePath.RoundCap
      strokeColor: mood.type === Mmood.MoodHapyHapy ? "#fff" : "transparent"
      strokeWidth: moodRect.width / 50
      PathMultiline {
        paths: [
          [ Qt.point(smile.yFactor / 2, -smile.yFactor / 2),
            Qt.point(-smile.yFactor / 2, smile.yFactor / 2) ],
          [ Qt.point(smile.width - smile.yFactor / 2, -smile.yFactor / 2),
            Qt.point(smile.width + smile.yFactor / 2, smile.yFactor / 2) ]
        ]
      }
    }
  }
}
