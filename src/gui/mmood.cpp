#include "mmood.h"

#include <QtCore/qdebug.h>


Mmood::Mmood(QObject *parent) :
  QObject{parent}
{
}


void Mmood::setType(MoodType mt) {
  if (m_type == mt)
    return;

  m_type = mt;
  emit typeChanged();
  qDebug() << "Mood type" << m_type;
}


