#ifndef MMOOD_H
#define MMOOD_H



#include <QtCore/qobject.h>
#include <QtQml/qqmlregistration.h>


class Mmood : public QObject
{

  Q_OBJECT
  QML_ELEMENT

  Q_PROPERTY(MoodType type READ type WRITE setType NOTIFY typeChanged)

public:
  explicit Mmood(QObject *parent = nullptr);

  enum MoodType {
    MoodHapyHapy,
    MoodHappy,
    MoodBalanced,
    MoodSad,
    MoodSadSad,

    MoodCount
  };
  Q_ENUM(MoodType)

  MoodType type() const { return m_type; }
  void setType(MoodType mt);

signals:
  void typeChanged();

private:
  MoodType                 m_type = MoodHapyHapy;

};

#endif // MMOOD_H
